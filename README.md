# pinta

Drawing/editing program modeled after Paint.NET. It's goal is to provide a simplified alternative to GIMP for casual users

https://pinta-project.com/

https://github.com/PintaProject/

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/rebornos-packages/graphics-audio-video/pinta.git
```

